# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/).


## [0.5.0] 2022 / 03 / 07

### Added

- `CloudShell` extension of kokiy to cloud points

### Changed

- `FrustumShell` now uses AxiShell as a basis
- normals now computed from coordinates
- some attributes moved to properties


## [0.4.0] 2021 / 11 / 25

### Added

- `TorchShell` more complex than a Frustum, for fire certification torches

## [0.3.1] 2021 / 11 / 24

### Fixed

- `FrustumShell` orthogonal direction computation


## [0.3.0] 2021 / 10 / 22


### Added

- `FrustumShell` object (still experimental)
- `Shell2D`'s `operate_on_shell_over_dirs` method: extends `average_on_shell_over_dirs` by allowing abstract operations


## [0.2.0] 2021 / 08 / 26

### Added

- `ThickShell` object
- `CakeDict` object (previously known as `cake`): for backwards compatibility.
- `.geo` export for mesh visualization using `tiny_3d_engine`
- mesh export in several formats via `yamio` (which uses `meshio` and `pyhip`)
- fields object to allow to store data in shell (and later dump)

### Changed

- transform properties in public attributes and remove corresponding private ones
- remove imports from `__future__`
- structure of class relationships:
    - Shell -> Shell2D -> AxiShell and CartShell 
    - Shell -> ThickShell

### Deprecated

- `shell_geom_type` and `shell_repr`
- `dump_shell`: use `dump` instead
- `u_adim` and `v_adim`: used `u` and `v` instead
- `CakeDict`: use `ThickShell` instead


### Removed

- `CartShell` initialization using control points


## [0.1.2] 2021 / 07 / 02

### Changed

- `infer_shell_from_xyz()` is now taking `bnd_uv` for limitation of axishell


## [0.1.1] 2021 / 06 / 17


### Fixed

- sign correction in computing tangential velocities
- recover the `bnd_angles` functionality when creating shells


## [0.1.0] 2021 / 06 / 03

### Added
  
- copy of the `Shell` utilities of `arnica` and `pyavbp`

