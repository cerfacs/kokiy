"""Regenerate files of tests..."""

from kokiy.axishell import AxiShell
from kokiy.frustum_shell import FrustumShell
from kokiy.cartshell import CartShell
from kokiy.torch_shell import TorchShell
from kokiy.thickshell import ThickShell


axish = AxiShell(8, 10, 40, (0.0, 0.2), (0.12, 0.12))

frustsh = FrustumShell(8, 10, (1.0, 1.0, 1.0), (2.0, 3.0, 4.0), 1.0, 0.2)
cartsh = CartShell(8, 10, [0.00, 0.12, -0.2], [0.00, 0.12, 0.2], [0.20, 0.12, -0.2])

torchsh = TorchShell(
    8, 10, (0.0, 0.0, 0.0), (0.0, 0.0, 1.0), 0.3, 1.2, (-0.5, 0.0, 1.0)
)


axish.dump(filename="axishell")
frustsh.dump(filename="frustshell")
cartsh.dump(filename="cartshell")
torchsh.dump(filename="torchshell")


# test_thickshell
tsh = ThickShell.bake_from_shell(axish, ((0.5, -0.04),), n_layers=4)
tsh.dump(filename="thickaxishell")

tsh = ThickShell.bake_from_shell(torchsh, ((0.5, -0.04),), n_layers=4)
tsh.dump(filename="thicktorchshell")


# test_shell
axish.export_geo(filename="axishell.geo")
frustsh.export_geo(filename="frustshell.geo")
cartsh.export_geo(filename="cartshell.geo")
torchsh.export_geo(filename="torchshell.geo")

tsh = ThickShell.bake_from_shell(axish, ((0.5, -0.04),), n_layers=2)
tsh.export_geo(filename="thickaxishell_neg_2.geo")
tsh.export_geo(filename="thickaxishell_neg_2_all.geo", show_all=True)

tsh = ThickShell.bake_from_shell(axish, ((0.5, -0.04),), n_layers=3)
tsh.export_geo(filename="thickaxishell_neg_3.geo")
tsh.export_geo(filename="thickaxishell_neg_3_all.geo", show_all=True)

tsh = ThickShell.bake_from_shell(axish, ((0.5, 0.04),), n_layers=2)
tsh.export_geo(filename="thickaxishell_pos_2.geo")
tsh.export_geo(filename="thickaxishell_pos_2_all.geo", show_all=True)

tsh = ThickShell.bake_from_shell(axish, ((0.5, 0.04),), n_layers=3)
tsh.export_geo(filename="thickaxishell_pos_3.geo")
tsh.export_geo(filename="thickaxishell_pos_3_all.geo", show_all=True)
