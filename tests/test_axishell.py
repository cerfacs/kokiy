import numpy as np
import pytest
from kokiy.axishell import AxiShell


def test_angle_min():
    ctrl_pts_x = (0.0, 0.2)
    ctrl_pts_r = (0.12, 0.12)
    n_azi, n_long = 8, 10
    angle = 40.
    angle_min = 0.
    axishell = AxiShell(n_azi, n_long, angle, ctrl_pts_x, ctrl_pts_r,
                        angle_min=angle_min)

    assert pytest.approx( np.rad2deg(np.min(axishell.theta)), 0.0)
    assert pytest.approx( np.rad2deg(np.max(axishell.theta)), 40.0)

