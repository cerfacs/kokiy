
import os
import itertools
from distutils import dir_util

import numpy as np
import pytest
from pytest_lazyfixture import lazy_fixture

from kokiy.axishell import AxiShell
from kokiy.cartshell import CartShell
from kokiy.thickshell import ThickShell
from kokiy.frustum_shell import FrustumShell
from kokiy.torch_shell import TorchShell


@pytest.fixture(scope='module')
def datadir(tmpdir_factory, request):
    """
    Fixture responsible for searching a folder with the same name as test
    module and, if available, moving all contents to a temporary directory so
    tests can use them freely.
    """
    file_path = request.module.__file__
    test_dir, _ = os.path.splitext(file_path)
    dir_name = os.path.basename(test_dir)

    datadir_ = tmpdir_factory.mktemp(dir_name)
    dir_util.copy_tree(test_dir, str(datadir_))

    return datadir_


@pytest.fixture
def axishell():
    return 'axishell', AxiShell(8, 10, 40, (0.0, 0.2), (0.12, 0.12))


@pytest.fixture
def cartshell():
    return 'cartshell', CartShell(8, 10, [0.00, 0.12, -0.2], [0.00, 0.12, 0.2], [0.20, 0.12, -0.2])


@pytest.fixture
def frustum_shell():
    return 'frustumshell', FrustumShell(
        8, 10, (1., 1., 1.), (2., 3., 4.),
                                        1., 0.2)


@pytest.fixture
def torch_shell():
    return 'torchshell', TorchShell(8, 10, (0., 0., 0.), (0., 0., 1.), 0.3,
                                    1.2, (-0.5, 0., 1.))


@pytest.fixture(params=lazy_fixture(['cartshell', 'axishell', 'frustum_shell',
                                     'torch_shell']))
def thickshell(request):
    shell_type_2d, shell_2d = request.param
    shell_type = f'thick{shell_type_2d}'
    return shell_type, ThickShell.bake_from_shell(shell_2d, ((0.5, -0.04),), n_layers=4)


@pytest.fixture(params=list(itertools.product([-1, 1], [2, 3])))
def param_thickaxishell(axishell, request):
    """Builds a thickshell (from a axishell) with different parameters.

    Because sometimes we just need to test Thickshell parameters without caring
    about shape of original 2d shell.
    """
    sign2text = {-1: 'neg', 1: 'pos'}
    shell_type_2d, shell_2d = axishell
    sign, n_layers = request.param
    width_profile = ((0.5, sign * 0.04),)
    shell_type = f'thick{shell_type_2d}_{sign2text[sign]}_{n_layers}'
    return shell_type, ThickShell.bake_from_shell(shell_2d, width_profile,
                                                  n_layers=n_layers)


@pytest.fixture(params=lazy_fixture(['cartshell', 'axishell', 'frustum_shell',
                                     'torch_shell']))
def shell_2d(request):
    return request.param
