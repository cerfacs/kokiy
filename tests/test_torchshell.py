
import pytest
import numpy as np

from kokiy.torch_shell import TorchShell


def test_ecc_validation():
    n_azi, n_longi = 8, 10
    pt_left = np.array((0., 0., 0.))
    pt_right = np.array((0., 0., 1.))
    radius_left = 0.3
    radius_right = 1.2

    # null exception
    ecc_pt = np.array((0., 0., 1.))
    with pytest.raises(Exception):
        TorchShell(n_azi, n_longi, pt_left, pt_right, radius_left,
                   radius_right, ecc_pt)

    # not perpendicular exception
    ecc_pt = np.array((1., 0., 0.))
    with pytest.raises(Exception):
        TorchShell(n_azi, n_longi, pt_left, pt_right, radius_left,
                   radius_right, ecc_pt)
