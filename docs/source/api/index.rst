
.. _api:


=============
API Reference
=============

.. currentmodule:: kokiy

.. autosummary::
   :nosignatures:
   :toctree: _generated/
   :template: module.rst

   shell
   shell_2d
   axishell
   cartshell
   thickshell
   frustum_shell
   torch_shell
   shell_utils

