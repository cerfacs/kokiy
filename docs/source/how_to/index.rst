
.. _how_to:

=============
How-To Guides
=============


.. toctree::
    :maxdepth: 1
    
    create_axishell
    create_cartshell
    create_frustum_shell
    create_torch_shell
    create_thickshell
    interpolate_random_solution
    set_mask_on_shell
    average_on_shell
    dump_export_shell
    
