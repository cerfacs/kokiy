How to create a cartesian shell?
================================

A ``CartShell`` object represents a cartesian computational shell. To create a ``CartShell`` you need to define its shape (number of azimuthal and longitudinal points) and three corners. The shell is built by creating two vectors based on the defined corners.


.. jupyter-execute::

    import numpy as np

    from kokiy.cartshell import CartShell

    n_trans = 6
    n_longi = 12
    zero = np.array([0.00, 0.12, -0.2])
    u_max = np.array([0.00, 0.12, 0.2])
    v_max = np.array([0.20, 0.12, -0.2])

    shell = CartShell(n_trans, n_longi, zero, u_max, v_max)



The following interactive plot shows the generated geometry:


.. jupyter-execute::
    :hide-code:

    import pyvista as pv
    from pyvista.utilities.fileio import from_meshio
    pv.set_jupyter_backend('ipygany')  
    pv.global_theme.background = 'white'

    yamio_mesh = shell.get_mesh('quad')
    mesh = from_meshio(yamio_mesh)


    plotter = pv.Plotter(notebook=True)
    plotter.add_mesh(mesh, color='tan')
    plotter.view_xz()
    plotter.camera.azimuth = 45

    plotter.show()