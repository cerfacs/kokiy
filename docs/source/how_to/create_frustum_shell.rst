How to create a frustum shell?
==============================

A ``FrustumShell`` object is a particular case of an axisymmetric shell where the shell crest variation is linear (depends on the left and right circles radii) and the revolution is complete. This shell is defined by two points representing the center of two circles and the respective radii.


.. jupyter-execute::
      
    from kokiy import FrustumShell

    n_azi, n_longi = 24, 10
    point_left = (0., 0., 0.)
    point_right = (1., 0., 0.)
    radius_left = .5
    radius_right = .1

    shell = FrustumShell(n_azi, n_longi, point_left, point_right, 
                         radius_left, radius_right)


The following interactive plot shows the generated geometry:

.. jupyter-execute::
    :hide-code:

    import pyvista as pv
    from pyvista.utilities.fileio import from_meshio
    pv.set_jupyter_backend('ipygany')  
    pv.global_theme.background = 'white'

    yamio_mesh = shell.get_mesh('quad')
    mesh = from_meshio(yamio_mesh)


    plotter = pv.Plotter(notebook=True)
    plotter.add_mesh(mesh, color='tan')

    plotter.show()


.. Tip::
    
    ``direc_pt`` allows to control which line (perpendicular to the main axis) has ``theta`` equal to zero.