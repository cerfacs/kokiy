How to create a torch shell?
============================


A ``TorchShell`` is a complex object defined by two points representing the center of two circles and the respective radii. The caveat is that in one of the faces there's an eccentricity that splits the circle into two semi-circles and a rectangle.


.. jupyter-execute::
      
    import numpy as np
    from kokiy import TorchShell

    n_azi, n_longi = 48, 5
    pt_left = np.array((0., 0., 0.))
    pt_right = np.array((0., 0., 1.))
    radius_left = 0.3
    radius_right = 1.2
    ecc_pt = np.array((.5, 0., 1.))

    shell = TorchShell(n_azi, n_longi, pt_left, pt_right, radius_left,
                       radius_right, ecc_pt)

The following interactive plot may shed some light on the generated geometry:

.. jupyter-execute::
    :hide-code:

    import pyvista as pv
    from pyvista.utilities.fileio import from_meshio
    pv.set_jupyter_backend('ipygany')  
    pv.global_theme.background = 'white'

    yamio_mesh = shell.get_mesh('quad')
    mesh = from_meshio(yamio_mesh)


    plotter = pv.Plotter(notebook=True)
    plotter.add_mesh(mesh, color='tan')

    plotter.show()


Notice several points collapse in the side opposite to the eccentricity.
