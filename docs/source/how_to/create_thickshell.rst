How to create a thick shell?
============================


The easiest way to create a ``ThickShell`` is to bake it from an already existing 2D shell (after defining the width profile). Here an example:

.. jupyter-execute::
   
    from kokiy import AxiShell
    from kokiy import ThickShell

    ctrl_pts_x = (0.0, 0.2)
    ctrl_pts_r = (0.12, 0.12)
    angle = 40
    n_azi, n_long = 8, 10
    shell_2d = AxiShell(n_azi, n_long, angle, ctrl_pts_x, ctrl_pts_r)

    width_profile = ((0.5, -0.04),)
    shell = ThickShell.bake_from_shell(shell_2d, width_profile, n_layers=4)


The following interactive plot shows the generated geometry:

.. jupyter-execute::
    :hide-code:

    import pyvista as pv
    from pyvista.utilities.fileio import from_meshio
    pv.set_jupyter_backend('ipygany')
    pv.global_theme.background = 'white'


    yamio_mesh = shell.get_mesh('hexahedron')
    mesh = from_meshio(yamio_mesh)


    plotter = pv.Plotter(notebook=True)
    plotter.add_mesh(mesh, color='tan')
    plotter.view_xy()
    plotter.camera.azimuth = 45
    plotter.camera.elevation = 25

    plotter.show()
