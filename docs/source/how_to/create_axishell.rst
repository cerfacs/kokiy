How to create a x-axisymmetric shell?
=====================================


An ``AxiShell`` objects represents an axisymmetric computational shell. To define an ``AxiShell`` you need to define its shape (number of azimuthal and longitudinal points) and control points (in ``x`` and ``r``). The shell is built by the creation of a spline.


.. jupyter-execute::
   
    from kokiy import AxiShell

    ctrl_pts_x = (0.0, 0.2)
    ctrl_pts_r = (0.12, 0.12)
    angle = 40
    n_azi, n_long = 8, 10

    shell = AxiShell(n_azi, n_long, angle, ctrl_pts_x, ctrl_pts_r)



The following interactive plot shows the generated geometry:

.. jupyter-execute::
    :hide-code:

    import pyvista as pv
    from pyvista.utilities.fileio import from_meshio
    pv.set_jupyter_backend('ipygany')  
    pv.global_theme.background = 'white'

    yamio_mesh = shell.get_mesh('quad')
    mesh = from_meshio(yamio_mesh)


    plotter = pv.Plotter(notebook=True)
    plotter.add_mesh(mesh, color='tan')
    plotter.view_xy()
    plotter.camera.azimuth = 45
    plotter.camera.elevation = 25

    plotter.show()


.. Tip::

    ``angle_min`` controls the minimum angle of the shell.
