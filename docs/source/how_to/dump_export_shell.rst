.. |paraview| replace:: ParaView
.. _paraview : https://www.paraview.org/

.. |tiny_3d_engine| replace:: ``tiny-3d-engine``
.. _tiny_3d_engine : https://pypi.org/project/tiny-3d-engine/


.. |fenics| replace:: FEniCS
.. _fenics : https://fenicsproject.org/


.. |yamio| replace:: ``yamio``
.. _yamio : https://pypi.org/project/yamio/


.. |meshio| replace:: ``meshio``
.. _meshio : https://pypi.org/project/meshio


.. |pyhip| replace:: ``pyhip``
.. _pyhip : https://pypi.org/project/pyhip



How to dump a shell and export a mesh?
======================================


Commonly, we want to be able to use the objects we've created within other softwares. This how-to shows how to serialize a ``Shell`` object for visualization in |paraview|_ (``dump``), scientific computation (e.g. with |fenics|_; ``export_mesh``) and visualization with |tiny_3d_engine|_ (``export_geo``).



Dump a shell (``dump``)
-----------------------

To dump a shell and its corresponding geometrical variables, just do:

.. code-block:: python
    
    shell.dump(filename)


If you have associated fields, you can also pass them, via a ``dict``. e.g:

.. code-block:: python

    my_fields = {'T': temperature}
    shell.dump(filename, fields=my_fields)


.. Note::
    Each field must have the shape of the ``shell`` instance.



Additionally, you can add the fields to the ``shell`` object and they will be automatically dumped.


.. code-block:: python
    
    shell.fields['T'] = temperature
    shell.dump(filename)



Dumps creates two files (`xmf` and `h5`). To open the `xmf` file in |paraview|, use ``Xdmf Reader``.


Export a mesh (``export_mesh``)
-------------------------------

``kokiy`` relies on |yamio|_ for mesh exporting. Therefore, you can export a mesh with any of the available formats in the package (basically everything available in |meshio|_ and |pyhip|_). The extension of your file indicates the format to which export.

To export to ``hip``:

.. code-block:: python
    
    filename = 'shell.mesh.xmf'
    shell.export_mesh(filename, elem_type='hexahedron')


This is an example output:

.. figure:: images/export_mesh_thick.png
    :align: center


.. Warning::

    ``hip`` cannot handle 3D objects that live in a 2D manifold (any ``Shell2D``). Therefore, you can only export volumetric objects with this format.


.. Note::
    
    Check out API Reference for all the available element types and |yamio|_ for all the available file formats.



Export a simplified ``.geo`` file (``export_geo``)
--------------------------------------------------

|tiny_3d_engine| is very neat for quick validations of the objects we've created (besides, it can be integrated in GUIs). To export a simplified ``.geo`` mesh:


.. code-block:: python

    filename = 'shell.geo'
    shell.export_geo(filename, show_all=False)


This is the corresponding output:

.. figure:: images/tiny_3d_engine.png
    :align: center



If ``show_all=True``, all the edges of the boundary surfaces are displayed.

.. figure:: images/tiny_3d_engine_all.png
    :align: center



Due to the limitations of |tiny_3d_engine| (remember it is tiny!), sometimes it may be better to export a shell with less elements. The ``replicate`` method is of great help here:


.. code-block:: python

    new_shape = (3, 6, 4)
    new_shell = shell.replicate(new_shape)
    new_shell.export_geo(filename, show_all=True)


.. figure:: images/tiny_3d_engine_replicate.png
    :align: center


.. Tip::

    Avoid reducing too much the shape of the shell, otherwise it will be unrecognizable (specially if it has a more complex shape).

