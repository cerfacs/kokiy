===================
kokiy documentation
===================

**Date**: |today| **Version**: |release|

**Useful links**: `Source Repository <https://gitlab.com/cerfacs/kokiy>`_ | `Bug Tracker <https://gitlab.com/cerfacs/kokiy/-/issues>`_ | `PyPI <https://pypi.org/project/kokiy/>`_


.. include:: ../../README.rst



.. toctree::
    :maxdepth: 1
    :hidden:
    
    how_to/index
    api/index
    changelog_link